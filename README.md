
### Problem Statement :-
> Find a website with public profiles and write a scraper to scrape those profiles, Save the output in Json, Csv, HTML.

Website used to scrap data :-
> https://friendorfollow.com/twitter/most-followers/
( This is a website which lists TOP 100 Twitter profiles )

#### Scraping :-
Web scraping is the process of extracting data from websites, and the script which is used to do scraping is called as an web scrapper.

#### Requirements :-
- BeautifulSoup 
- requests
- json
- csv
- sys
- time

#### USAGE :-

> python3 scrapper.py <n> <file_type>
n = Number of profiles to scrap (1-100)
file_type = Save output as :- default/JSON/CSV/HTML 
(default will print the output to stdout)
File is saved it as output.json/output.csv/output.html in the same directory.

#### Example :-
- python3 scrapper.py 2 default
![](https://raw.githubusercontent.com/rohanchavan1918/scrapper/master/default_example.PNG?token=AGEZCS5TGAPPMJIHGYY4KWK5NZOTK)
- python3 scrapper.py 5 json
![](https://github.com/rohanchavan1918/scrapper/blob/master/json_output.PNG?raw=true)
- python3 scrapper.py 5 html
![](https://github.com/rohanchavan1918/scrapper/blob/master/html_output.PNG?raw=true)
- python3 scrapper.py 5 csv
![](https://github.com/rohanchavan1918/scrapper/blob/master/csv_output.PNG?raw=true)



