'''
@Author = Rohan Chavan
Email :- rohanchavan1918@gmail.com
Github :- https://github.com/rohanchavan1918/
		  https://github.com/p5yph3r/
website :- https://p5yph3r.github.io/

Problem Statement :- Find a website with public profiles and write a scraper to scrape those profiles
'''


import time
start_time = time.time()
import sys
from bs4 import BeautifulSoup
import requests
import json
import csv

# Implementing a User Class for simplicity
class User:
	def __init__(self,twitter_user_data):
		self.twitter_user_data = twitter_user_data

	# Returns Real Name of User
	def get_real_name(self):
		# <h3>1. Barack Obama</h3>
		self.name = self.twitter_user_data.find_all('h3')
		for f3 in self.name:
			user = f3.text
		return(str(user))

	# Returns Users Twitter Handle
	def get_twitter_username(self):
		# <a target="f" class="tUser" href="http://twitter.com/BarackObama">@BarackObama</a>
		for twitter_user_name in self.twitter_user_data.findAll('a', attrs={"class": "tUser"}):
		    username = twitter_user_name.text
		return(str(username))

	# Returns BIO and Location of User
	def get_bio_location(self):
		bios = []
		# <p class="list-bio-loc"><strong>Bio:</strong> Dad, husband, President, citizen.</p>
		for tweet in self.twitter_user_data.findAll('p', attrs={"class": "list-bio-loc"}):
		    bios.append(tweet.text)
		try:
			bio = str(bios[0]) # first element is Bio always
		except:
			bio = "location: Not Available"
		# <p class="list-bio-loc"><strong>Location:</strong> Washington, DC</p>
		try:	
			location = str(bios[1]) # second is always Location
		except:
			location = "location: Not Available"
		return bio,location

	# Returns Follower and Following Number
	def get_user_follow_info(self):
		''' Scrapping from this Image tag
		                             <img width="73" height="73"
                                    name="katy perry"
                                    location=""
                                    following="221"
                                    followers="107942796"

                                    src="https://pbs.twimg.com/profile_images/1159677711314386944/-3CQtnBa_bigger.jpg"
                                    onerror="e(this,21447363)"
                                    alt="katyperry"
                                    class="img-polaroid hc " />
        '''
		followers = self.twitter_user_data.find('img', attrs={"class": "img-polaroid hc "})['followers']
		following = self.twitter_user_data.find('img', attrs={"class": "img-polaroid hc "})['following']
		return followers,following

	# Returns Number of tweets of a user.
	def get_user_tweets(self):
		#  <td><strong>tweets</strong><br />10,065</td>
		pop =  self.twitter_user_data.findAll('td')
		main_str = (pop[2].get_text())
		return str(main_str[6:])

def master_iterator(response_text,N,file_type):
	# This Function will just iterate for N times, will return top N number of Users.
	for i in range(0,N+1):
		var = "row-"+str(i) # <li class="row-1  ver " id="list-813286">
		for tweet in content.findAll("li", attrs={'class': var}):
			temp_user = User(tweet)
			real_name = temp_user.get_real_name()
			twitter_user_name = temp_user.get_twitter_username()
			twitter_user_bio, twitter_user_location = temp_user.get_bio_location()
			twitter_user_follower, twitter_user_following = temp_user.get_user_follow_info()
			twitter_user_tweets = temp_user.get_user_tweets()

			# default will just print the output on the CMD/Terminal.
			if file_type == "default":
	#			print(real_name,"->",twitter_user_name,"->",twitter_user_follower,"->", twitter_user_following,"->", twitter_user_bio,"->",twitter_user_location,"->", twitter_user_tweets)
				print("real_name :-",temp_user.get_real_name())
				print("twitter_user_name :-", temp_user.get_twitter_username())
				print("twitter_user_bio :-", twitter_user_bio)
				print("twitter_user_location :-", twitter_user_location)
				print("twitter_user_follower :-", twitter_user_follower)
				print("twitter_user_following :-", twitter_user_following)
				print("twitter_user_tweets :-",temp_user.get_user_tweets())
				print("##################################################################")

			# JSON data coule be easily used any where, this gives flexiblity to our script.	
			elif file_type == "json":
				tweet_object = {
					"real_name":temp_user.get_real_name(),
					"twitter_user_name": temp_user.get_twitter_username(),
					"twitter_user_bio":twitter_user_bio,
					"twitter_user_location": twitter_user_location,
					"twitter_user_follower": twitter_user_follower,
					"twitter_user_following": twitter_user_following,
					"twitter_user_tweets":temp_user.get_user_tweets(),
					}
				
				with open('output.json', 'a+',encoding="utf-8") as outfile:
					json.dump(tweet_object, outfile, sort_keys = True, indent = 4,ensure_ascii = False)

			# Export output to a CSV file so that it coule be used further by NOn technical people.
			elif file_type == "csv":
				row = [temp_user.get_real_name(),temp_user.get_twitter_username(),twitter_user_bio,twitter_user_location,twitter_user_follower,twitter_user_following,temp_user.get_user_tweets()]
				with open('output.csv','a+') as csvFile:
					writer = csv.writer(csvFile)
					writer.writerow(row)
				csvFile.close()

			# HTML is the best way to view the output, as the data is UTF-8 and has emojis and arabic contents.
			elif file_type == "html":
				half1 = "Name :-"+temp_user.get_real_name()+"&nbsp;&nbsp; Twitter Handle :-"+temp_user.get_twitter_username()+" &nbsp;&nbsp; Followers"+str(twitter_user_follower)+" &nbsp;&nbsp; Following :-"+str(twitter_user_following)+"<br>"
				half2 = "Bio :-"+twitter_user_bio+"&nbsp; &nbsp; Location :-"+twitter_user_location+"<br><br>"
				print(half1)
				print(half2)
				with open('output.html','a+',encoding="utf-8") as htmlFile:
					htmlFile.write(half1)
					htmlFile.write(half2)




def help():
	print("[?] USAGE :-")
	print("[>] python scrapper.py <n> <file_type>")
	print("--------------------------------------")
	print("[+] N - Number of Top profiles")
	print("[+] file_type = default/csv/html/json")

if len(sys.argv) < 3:
	help()
	sys.exit()

N = int(sys.argv[1])
file_type = sys.argv[2]
print(" [+] Scrapping Top ",N," Twitter Profiles From \n https://friendorfollow.com/twitter/most-followers/ \n")

url = "https://friendorfollow.com/twitter/most-followers/" #url to scrape
try:
	response = requests.get(url,timeout=10) 
	global content
	content = BeautifulSoup(response.content,"html.parser") # HTML Content acquired
	tweet = master_iterator(response.text,N,file_type)
	print("Scraped ",N," Users Data in")
	print("--- %s seconds ---" % (time.time() - start_time))
except:
	print("[!] Some ERROR OCCOURED, Check Internet Connection")
